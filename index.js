require('dotenv').config()
const axios = require('axios');
var moment = require('moment');
const fs = require('fs');

const DATE_TIME_FORMAT = "YYYY-MM-DDTHH:mm:ss"

async function getTrafficDataLatLon (dateTime) {
    return new Promise((resolve, reject) => {
        axios.get("https://api.data.gov.sg/v1/transport/traffic-images?date_time=" + encodeURIComponent(dateTime))
            .then(res => {
                if (res.data.items.length > 0) {
                    resolve(res.data.items[0].cameras.map(camera => { return { lat: camera.location.latitude, lon: camera.location.longitude } }))
                } else {
                    resolve([])
                }

            }).catch(err => {
                reject(err)
            })
    })
}

async function getWeatherData (dateTime) {
    return new Promise((resolve, reject) => {
        axios.get("https://api.data.gov.sg/v1/environment/2-hour-weather-forecast?date_time=" + encodeURIComponent(dateTime))
            .then(res => {
                if (res.data.area_metadata.length > 0) {
                    resolve(res.data.area_metadata.map(area => { return { lat: area.label_location.latitude, lon: area.label_location.longitude } }));
                } else {
                    resolve([])
                }
            }).catch(err => {
                reject(err)
            })
    })
}

async function geocodeLatLon (lat, lon) {
    console.log("Geocoding: ", lat, lon)
    return new Promise((resolve, reject) => {
        axios.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key=" + process.env.GOOGLE_API_KEY)
            .then(res => {
                resolve(res.data)
            }).catch(err => {
                reject(err)
            })
    })
}

function compareLatLon (latlon1, latlon2) {
    if (latlon1.lat === latlon2.lat && latlon1.lon === latlon2.lon) {
        return true;
    } else {
        return false;
    }
}

function makeListUnique (array1, comparator) {
    var a = array1.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (comparator(a[i], a[j]))
                a.splice(j--, 1);
        }
    }

    return a;
}

Array.prototype.unique = function () {

};

// Main
(async () => {
    let startDate = moment()

    let listOfLatLons = []

    for (let index = 0; index < 5; index++) {
        let latlons = await getTrafficDataLatLon(startDate.format(DATE_TIME_FORMAT));
        listOfLatLons = listOfLatLons.concat(latlons)

        let weatherLatlons = await getWeatherData(startDate.format(DATE_TIME_FORMAT));
        listOfLatLons = listOfLatLons.concat(weatherLatlons)

        console.log(startDate.format("YYYY-MM-DD") + " " + listOfLatLons.length)
        startDate.subtract(1, 'days');
    }

    let array = makeListUnique(listOfLatLons, compareLatLon);

    let data = JSON.stringify(array);
    fs.writeFileSync('latlons.json', data);

    let existingLatLons = fs.readFileSync('geocode_mapping.json');
    let existingLatLonsList = JSON.parse(existingLatLons);

    let arrayEverything = makeListUnique(existingLatLonsList.concat(array), compareLatLon);

    let geocodes = arrayEverything.filter((element) => element.geocodeResponse)

    array = arrayEverything.filter((element) => element.geocodeResponse === undefined)

    await Promise.all(array.map(async element => {
        let response = await geocodeLatLon(element.lat, element.lon);
        let newEntry = { lat: element.lat, lon: element.lon, geocodeResponse: response }
        geocodes.push(newEntry)
    }));

    let data2 = JSON.stringify(geocodes);
    fs.writeFileSync('geocode_mapping.json', data2);

    // Extract only relevant info
    let extractedData = geocodes.map(geocode => {
        let results = geocode.geocodeResponse.results
        let addressName = "";
        if (results.length > 0) {
            results.forEach(result => {
                if (result.types.includes("route")) {
                    addressName = result.formatted_address;
                }
            });

            // Just get the most accurate one if there is no route
            if (addressName === "") {
                addressName = results[0].formatted_address;
            } else {
                // Provide more detail if possible
                for (let index = 0; index < results.length; index++) {
                    const element = results[index];
                    if (element.types.includes("street_address")) {
                        addressName += " (" + element.formatted_address + ")";
                        break;
                    }
                }
            }
        }
        return { lat: geocode.lat, lon: geocode.lon, addressName: addressName }
    })

    fs.writeFileSync('geocode_mapping_extracted.js', "export const GEOCODE_MAPPING = " + JSON.stringify(extractedData));

})();