# take-home-technical-assessment-geocoder

## How to run the source codes
1) Check out this repository

```
git clone https://gitlab.com/wengpaul/take-home-technical-assessment-geocoder
```

2) Install npm dependencies

```
cd take-home-technical-assessment-geocoder
npm install
```

3) Copy the .env file which contains the google api key to the root folder or create one with the follow in the .env file
```
GOOGLE_API_KEY={Your API KEY}
```

4) Run the app in the development mode

```
npm start
```

## To update the reverse geocode mappings in the frontend
Run the code to generate the following file
`geocode_mapping_extracted.js` 

Copy the `geocode_mapping_extracted.js` to the frontend project.
